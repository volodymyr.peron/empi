# -----------------------For Windows OS-----------------------

# 1 step
Disable Windows subsystem for Linux in Windows.

# 2 step
Add "..\Git\bin" to Path variable.

# 3 step
Set path (linux style) to your project folder in file "project.json".

# 4 step
Run project.

# 5 step
Open "output.xlsx" for viewing result.